<?php
namespace App\Services;
use App\Interfaces\CacheInterface;

class Cache implements CacheInterface
{

  public function _construct() {

  }

  public function set(string $key, $value, int $duration){
    if($value) {
      $updatedValue = json_decode($value, true);
      $updatedValue['duration'] = $duration;
      $finalData[$key] = $updatedValue;
    } else {
      $finalData[$key] = null;
    }
    $finalData = json_encode($finalData);
    if(file_put_contents($this->generateFileName($key), $finalData)) {
      return $this->get($key);
    }
  }


  public function get(string $key){
    $filename = $this->generateFileName($key);
    $data = json_decode(file_get_contents($filename), true);
    if(isset($data[$key]) && $data[$key]) {
      if(isset($data[$key]['duration']) && ((int)$data[$key]['duration'] < time() - filemtime($filename))) {
        $this->set($key, null, 0);
      }
    }
    return $data[$key];
  }

  public function generateFileName($name) {
    return $name. ".json";
}

}
