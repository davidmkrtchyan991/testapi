<?php

namespace App;

use App\Interfaces\CacheInterface;
use GuzzleHttp\Client;

class DataRetrieve
{
  /**
   * @var $cache CacheInterface
   */
  public $cache;

  private $ratesEndpoint = "https://api.printful.com/shipping/rates";
  private $apiKey = "77qn9aax-qrrm-idki:lnh0-fm2nhmp0yca7";


  public function __construct(CacheInterface $cache)
  {
    $this->cache = $cache;
  }


  public function render(string $key)
  {
    return (!file_exists($key.".json")) ? $this->cache->set($key, $this->requestApi(), 300) : $this->cache->get($key);
  }

  public function requestApi()
  {
    $credential = base64_encode($this->apiKey);
    $client = new Client([
      "headers" => [
        "Content-Type" => "application/json",
        "Authorization" => "Basic " . $credential,
      ]
    ]);

    $response = $client->post($this->ratesEndpoint,
      ["body" => json_encode(
        [
          "recipient" => [
            "address1" => "11025 Westlake Dr, Charlotte, North Carolina, 28273",
            "country_code" => "US"
          ],
          "items" => [
            [
              "quantity" => 2,
              "variant_id" => 7679
            ]
          ]
        ]
      )]
    );

    return $response->getBody()->getContents();
  }

}
